import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { HomeRoutingModule } from "./home-routing.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { HomeComponent } from "./home.component";
import { ListaComponent } from "../lista/lista.component";
import { DetalleComponent } from "../detalle/detalle.component";
import { ListaFormComponent } from "../lista/lista-form.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        HomeRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        HomeComponent,
        ListaComponent,
        DetalleComponent,
        ListaFormComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HomeModule { }
