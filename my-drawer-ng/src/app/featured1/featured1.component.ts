import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'featured1',
  templateUrl: './featured1.component.html',
  moduleId: module.id,
})
export class Featured1Component implements OnInit {

  constructor(private routerExtensions: RouterExtensions) { }

  ngOnInit() {
  }


  goBack() {
    this.routerExtensions.backToPreviousPage();
  }

  onShare() {
    
  }

  onDelete() {

  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onDrawerButtonClick(): void {    
    this.routerExtensions.navigate(["/featured2"], {
      transition: {
          name: "fade"
      }
  });
  }

}
