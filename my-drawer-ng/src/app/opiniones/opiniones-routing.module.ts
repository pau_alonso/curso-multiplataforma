import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { OpinionesComponent } from "./opiniones.component";

const routes: Routes = [
    { path: "", component: OpinionesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class OpinionesRoutingModule { }
