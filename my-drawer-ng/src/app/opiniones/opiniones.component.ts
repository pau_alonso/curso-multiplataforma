import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { GestureEventData } from "@nativescript/core/ui/gestures/gestures";
import { Location } from "@angular/common";


@Component({
  selector: 'opiniones',
  templateUrl: './opiniones.component.html',
})
export class OpinionesComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
}
}
