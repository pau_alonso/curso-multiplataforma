import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { GestureEventData } from "@nativescript/core/ui/gestures/gestures";
import { Button } from "tns-core-modules/ui/button";

var count: number;

@Component({
    selector: "My Friends",
    templateUrl: "./my-friends.component.html"
})
export class MyFriendsComponent implements OnInit {

    constructor() {
      const myButton = new Button();
      myButton.text = "Tap me!";
      myButton.className = "btn btn-primary btn-active";
      myButton.on(Button.tapEvent, (data: GestureEventData) => {
          // data is of type GestureEventData
          alert("Button Tapped!");
      });
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}


